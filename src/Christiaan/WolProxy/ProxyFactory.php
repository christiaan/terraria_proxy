<?php
namespace Christiaan\WolProxy;

use React\EventLoop\LoopInterface;

class ProxyFactory
{
    private $loop;

    function __construct(LoopInterface $loop)
    {
        $this->loop = $loop;
    }

    public function proxyTo($host)
    {
        return new Proxy($this->loop, $host);
    }
}