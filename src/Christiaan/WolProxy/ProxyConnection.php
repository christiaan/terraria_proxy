<?php
namespace Christiaan\WolProxy;

use React\Socket\Connection;

class ProxyConnection
{
    /** @var \React\Socket\Connection */
    private $incoming;
    private $outgoing;
    private $initialData;

    public function __construct(Connection $incoming)
    {
        $this->incoming = $incoming;

        $this->initialData = '';
        $this->incoming->on('data', array($this, 'onInitialData'));
    }

    public function onInitialData($data)
    {
        $this->initialData .= $data;
    }

    public function connect(Connection $outgoing)
    {
        $this->outgoing = $outgoing;

        $this->incoming->removeAllListeners('data');

        $this->incoming->pipe($this->outgoing);
        $this->outgoing->pipe($this->incoming);

        $this->outgoing->write($this->initialData);
        $this->initialData = null;

        return $this;
    }

    public function isConnected()
    {
        return $this->outgoing instanceof Connection;
    }

    public function close()
    {
        $this->incoming->end();
        return $this;
    }
} 