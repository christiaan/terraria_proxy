<?php
namespace Christiaan\WolProxy;

use React\EventLoop\LoopInterface;
use React\Socket\Connection;
use React\Socket\Server;

class Proxy
{
    private $loop;

    private $host;
    private $mac;

    private $listeningOn;
    private $connections;

    private $lastWake;

    function __construct(LoopInterface $loop, $host)
    {
        $this->loop = $loop;
        $this->host = $host;

        $this->listeningOn = [];
        $this->connections = [];

        $this->lastWake = 0;
    }

    /**
     * @api
     * @param string $mac
     * @return Proxy
     */
    public function wakeAddress($mac)
    {
        $this->mac = $mac;
        return $this;
    }

    /**
     * @api
     * @param int $listenPort
     * @return proxy
     */
    public function listen($listenPort)
    {
        $socket = new Server($this->loop);

        echo 'Listening at port ' . $listenPort . PHP_EOL;
        $socket->listen($listenPort, '0.0.0.0');

        $socket->on('connection', array($this, 'onConnection'));

        $this->listeningOn[] = $socket;

        return $this;
    }

    public function onConnection(Connection $connection)
    {
        echo 'A player connected' . PHP_EOL;
        $proxy = new ProxyConnection($connection);

        $this->connections[] = $proxy;
        $this->proxyConnection($proxy);
    }

    private function proxyConnection(ProxyConnection $proxy, $tries = 0) {
        echo 'Connecting to terraria server at ' . $this->host . PHP_EOL;
        if ($client = $this->createClient()) {
            echo 'Connected, patching trough' . PHP_EOL;
            $proxy->connect($client);
            return;
        }

        if ($this->wakeHost($tries)) {
            $this->loop->addTimer(1, function() use($proxy, $tries) {
                    echo 'Go check if server is up already try ' . $tries . PHP_EOL;
                    $this->proxyConnection($proxy, $tries + 1);
                });
        } else {
            echo 'Tried and given up after ' . $tries . ' tries'. PHP_EOL;
            $proxy->close();
        }
    }

    private function createClient()
    {
        $client = @stream_socket_client($this->host, $errno, $errstr, 1);
        if (!is_resource($client)) {
            return null;
        }
        return new Connection($client, $this->loop);
    }

    private function wakeHost($tries)
    {
        if ($tries > 60 * 3) {
            return false;
        }

        if (!$this->lastWake || time() - $this->lastWake > 60 * 3) {
            echo 'Sending wake on lan to ' . $this->mac. PHP_EOL;
            exec('wakeonlan ' . escapeshellarg($this->mac), $_, $code);
            if ($code === 0) {
                $this->lastWake = time();
            }
        }
        return true;
    }
}