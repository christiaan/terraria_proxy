<?php
require __DIR__ . '/vendor/autoload.php';

$host = 'tcp://192.168.1.11:7777';
$mac = '00:21:85:10:CF:18';
$listenPort = 7777;

$loop = React\EventLoop\Factory::create();

$proxyFactory = new \Christiaan\WolProxy\ProxyFactory($loop);

$proxy = $proxyFactory->proxyTo($host)->wakeAddress($mac)->listen($listenPort);

$loop->run();